# ros2_perf_analysis

Investigation into ROS 2 performance.

## Introduction

After coming accross a [question on ROS answers](https://answers.ros.org/question/327477/ros2-uses-6-times-more-cpu-than-fastrtps/) asking why ROS 2 consumes much more CPU than Fast-RTPS alone, we started looking into it as well.

## First benchmark

We first reproduced the results described in the ROS answers post by running the same benchmark and looking at `htop`. The test nodes were compiled with `CMAKE_BUILD_TYPE=RelWithDebInfo`.


| Test node     | CPU usage | Notes |
|:--------------|:---------:|:------|
| rtps          | 17 %      | [Fast-RTPS Only](https://github.com/scgroot/ros2_performance/blob/ae5c2994f6ca571c14251f14445f49da36ed7f90/src/rtps.cc) |
| ros           | 70-80 %   | [ROS 2 on top of Fast-RTPS](https://github.com/scgroot/ros2_performance/blob/ae5c2994f6ca571c14251f14445f49da36ed7f90/src/ros.cc) |
| nopub         | 14-15 %   | [no pub/sub; 10 nodes with 1 timer each (20 ms)](https://github.com/scgroot/ros2_performance/blob/ae5c2994f6ca571c14251f14445f49da36ed7f90/src/noros.cc) |
| nopub_onenode | 2 %       | [no pub/sub; 1 node with 10 timer (20 ms)](https://github.com/christophebedard/ros2_performance/blob/ff248591c63de7bf2cc021de554ef91309165ca5/src/nopub_onenode.cc) |

There is indeed a big difference in CPU usage between `rtps` and `ros`. However, that doesn't say much about the possible cause.

Looking at `nopub` vs. `nopub_onenode`, we can see it has something to do with the number of nodes and how they are handled. Therefore, we should look into the ROS 2 executor. In this case, we're using the `SingleThreadedExecutor`.

## A word of caution about overhead

We initialy built the complete stack from source using `-finstrument-functions` to instrument function entries and exits, as shown below.

![](./images/example_overhead.png)

It looks like the `collect_entities()` function takes about half of the time under `wait_for_work()`. The overhead introduced by the function entry and exit instrumentation is the same for every function call, i.e. it's proportional to the number of function calls. Since `collect_entities()` has a lot of of function calls (mostly `shared_ptr` accesses through `weak_ptr`), the overhead skews the results.

If we simply add `-finstrument-functions-exclude-file-list=/usr/include/`, it will not generate instrumentation calls for functions defined in files under `/usr/include/`. This will remove a lot of the quick calls to the standard library. The result of this is shown below. It tells a very different story!

![](./images/example_overhead_less.png)

## Executor profiling analysis

See [`executor_profiling/post.md`](./executor_profiling/post.md).
