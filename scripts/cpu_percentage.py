#! /usr/bin/python3
#
# Copyright (c) 2019 Robert Bosch GmbH
#
# This script takes the output of "babeltrace", and a list of process names.
# It sums up the CPU usage of these executables and their various threads.
#
# NOTE: The process name reported by "babeltrace" is usually truncated. You
# need to specify the truncated name.


import numpy as np
import pandas as pd
import sys
import re

switch = re.compile(r"\[(.*)\].*sched_switch: .*prev_comm = \"(.*)\".*prev_tid = (\d+).*next_comm = \"(.*)\".*next_tid = (\d+)")

tids = {}

df = pd.DataFrame(columns=["procname", "tid", "start", "end"])

count = 0

procs = sys.argv[1:]
if not len(procs) > 1:
    print("Need at least one process name argument")
    sys.exit(-1)

for line in sys.stdin:
    match = switch.search(line)
    if match is not None:
        timestamp = match.group(1)
        prev_comm = match.group(2)
        prev_tid = match.group(3)
        next_comm = match.group(4)
        next_tid = match.group(5)

        if len(procs) > 0:
            if next_comm in procs:
                tids[next_tid] = timestamp
            elif prev_comm in procs:                
                if prev_tid in tids:
                    df.loc[count] = prev_comm, prev_tid, tids[prev_tid], timestamp
                    count += 1
                    del tids[prev_tid]
                else:
                    pass
                    #print(timestamp, prev_comm, "gone inactive without seeing active event")
            else:
                continue
        else:
            print(line)
    

df["start"] = pd.to_datetime(df["start"])
df["end"] = pd.to_datetime(df["end"])
# add duration column
df["duration"] = df["end"] - df["start"]

entire_duration = float((df.tail(1)["end"] - df.loc[0]["start"]) / np.timedelta64(1, 's'))
proc_duration = float(df["duration"].sum()/np.timedelta64(1, 's'))
print("Runtime: %.2fs, Process: %.2fs (%.2f%%)" % (entire_duration, proc_duration, proc_duration/entire_duration))

per_thread = df.groupby("tid").agg({"duration": "sum"})
per_thread["duration"] = per_thread["duration"]/np.timedelta64(1, 's')
per_thread["pct"] = (per_thread["duration"]/entire_duration)*100.0
per_thread["pct_proc"] = (per_thread["duration"]/proc_duration)*100.0
print(per_thread)

#print(df)