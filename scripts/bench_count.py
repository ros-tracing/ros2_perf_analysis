#! /usr/bin/python3
#
# Copyright (c) 2019 Robert Bosch GmbH
#
# This script takes the output of "babeltrace" and computes how often and 
# how long the instrumented functions run.
#
# You need to use this in conjunction with an executable that is instrumented
# with the approach from https://github.com/christophebedard/instrument-attribute-gcc-plugin
#


import pandas as pd
import sys
import re

m_entry=re.compile("\[(.*)\].*lttng_ust_cyg_profile_fast:func_entry.*func = \"(.*)\"")
m_exit=re.compile("\[(.*)\].*lttng_ust_cyg_profile_fast:func_exit.*func = \"(.*)\"")

entries={}

df = pd.DataFrame(columns=["funcname", "start", "end"])

count = 0

for line in sys.stdin:
    match = m_entry.search(line)
    if match is not None:
        timestamp = match.group(1)
        funcname = match.group(2)
        entries[funcname] = timestamp
    else:
        match = m_exit.search(line)
        if match is not None:
            timestamp = match.group(1)
            funcname = match.group(2)
            if funcname in entries:
                df.loc[count] = funcname, entries[funcname], timestamp
                count += 1
            else:
                print("Exit with no entry for", funcname)

# convert to timestamps in one go
df["start"] = pd.to_datetime(df["start"])
df["end"] = pd.to_datetime(df["end"])
# add duration column
df["duration"] = df["end"] - df["start"]

print(df[["funcname", "duration"]])
print(df[["funcname", "duration"]].groupby("funcname").describe())

