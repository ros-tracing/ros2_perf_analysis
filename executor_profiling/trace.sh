#!/bin/bash

lttng create profile-ros-instr-overall

lttng enable-channel -u ros --subbuf-size=8M
lttng enable-channel -k kernel --subbuf-size=8M
lttng enable-event -c ros -u lttng_ust_cyg_profile_fast:func_entry
lttng enable-event -c ros -u lttng_ust_cyg_profile_fast:func_exit
lttng enable-event -c ros -u lttng_ust_statedump:start
lttng enable-event -c ros -u lttng_ust_statedump:end
lttng enable-event -c ros -u lttng_ust_statedump:bin_info
lttng enable-event -c ros -u lttng_ust_statedump:build_id
lttng enable-event -c kernel -k sched_switch
lttng add-context -u -t vtid -t vpid -t procname -t ip
lttng start
LD_PRELOAD=/usr/lib/x86_64-linux-gnu/liblttng-ust-cyg-profile-fast.so ./build/ros_performance/ros
lttng stop
lttng destroy
